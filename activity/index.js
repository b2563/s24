const getCube = 2 ** 3
	console.log(`The cube of 2 is ${getCube}`)

    const address = {
		street: "258 Washington Ave",
		city: "NW",
		state: "California",
		zipCode: "90011"
	}

	const{street, city, state, zipCode} = address
	console.log(`I live at ${street} ${city} ${state} ${zipCode}`);

	const animal = {
		name: "Lolong",
		type: "saltwater crocodile",
		weight: "1075 kgs",
		measurement: "20 ft 3 in"
	}

	const{name, type, weight, measurement} = animal
	console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`);

	const numbers = [1, 2, 3, 4, 5]

	numbers.forEach(function(number) {
		console.log(number)
	})

	function add(a, b, c, d, e) {
		return a + b + c + d + e
	}
	let total = add(1, 2, 3, 4, 5)
	console.log(total)

	class Dog {
		constructor(name, age, breed) {
			this.name = name
			this.age = age
			this.breed = breed
		}
	}

	const myDog = new Dog()
	myDog.name = "Frankie"
	myDog.age = 5
	myDog.breed = "Miniature Duchhound"
	console.log(myDog);
